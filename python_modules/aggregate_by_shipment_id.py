#!/usr/bin/env python
# coding: utf-8

# ###### Import dependencies

# In[ ]:


from pyspark.sql           import SparkSession
from pyspark.sql.functions import col, sum
from config                import config

import sys

sys.path.insert(1, '../python_modules/configs')


# ###### Create SparkSession

# In[ ]:


# This example is for a Hadoop-based installation.
# If in Azure Databricks, this is provided as part of the 
# Databricks Runtime environment.
spark = (
  SparkSession
    .builder
    .appName("basic_aggregation")
    .config("spark.executor.cores",
            "5")
    .config("spark.executor.memory",
            "10G")
    .config("spark.executor.memoryOverhead",
            "2G")
    .getOrCreate()
)


# ###### Local variables

# In[ ]:


inputDataDir  = config["input_data_dir"]
outputDataDir = config["output_data_dir"]

print(f"inputDataDir:  {inputDataDir}")
print(f"outputDataDir: {outputDataDir}")


# ###### Import Data

# In[ ]:


myDF = (
  spark
    .read
    .parquet(inputDataDir)
)

print(f"myDF count: {myDF.count()}")


# ###### Implementation

# In[ ]:


sumCostsWithAccessorialsPerShipmentDF = (
  myDF
    .groupBy("shipment_id")
    .agg(sum("shipment_cost")
           .alias("sum_shipment_cost"),
         sum("shipment_accessorial_cost")
           .alias("sum_shipment_accessorial_cost"))
    .withColumn("sum_total_shipment_cost",
                col("sum_shipment_cost") + col("sum_shipment_accessorial_cost"))
    .select("sum_shipment_cost",
            "sum_shipment_accessorial_cost",
            "sum_total_shipment_cost")
)

print(f"sumCostsWithAccessorialsPerShipmentDF count: {sumCostsWithAccessorialsPerShipmentDF.count()}")


# ###### Another way to do this...

# In[ ]:


(
  myDF
    .createOrReplaceTempView("my_df")
)

sumCostsWithAccessorialsPerShipmentDF = (
  spark
    .sql("""   SELECT SUM(shipment_cost)                                  AS sum_shipment_cost
                    , SUM(shipment_accessorial_cost)                      AS sum_shipment_accessorial_cost
                    , (sum_shipment_cost + sum_shipment_accessorial_cost) AS sum_total_shipment_cost
                 FROM my_df
             GROUP BY shipment_id
         """)
)

print(f"sumCostsWithAccessorialsPerShipmentDF count: {sumCostsWithAccessorialsPerShipmentDF.count()}")


# ###### Write Output

# In[ ]:


(
  sumCostsWithAccessorialsPerShipmentDF
    .write
    .mode("overwrite")
    .parquet(outputDataDir)
)

