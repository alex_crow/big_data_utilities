#!/bin/bash

JDBC_STRING=${1}
USERNAME=${2}
PASSWORD_FILE_LOCATION=${3}
INPUT_TABLE=${4}
INPUT_FILTER=${5}
LANDING_DIR=${6}
NUM_MAPPERS=${7}

if [ -n "${INPUT_FILTER}" ]; then
  INPUT_FILTER="1 = 1"
fi

sqoop import                                  \
  --connect       "${JDBC_STRING}"            \
  --username      "${USERNAME}"               \
  --password-file "${PASSWORD_FILE_LOCATION}" \
  --query         "SELECT *                   \
                     FROM ${INPUT_TABLE}      \
                    WHERE ${INPUT_FILTER}     \
                      AND \$CONDITIONS"       \
  --hive-import                               \
  --hive-table    "${INPUT_TABLE}"            \
  --target-dir    "${LANDING_DIR}"            \
  -m              "${NUM_MAPPERS}"
RETURN_CODE=${?}

if [ "${RETURN_CODE}" -ne 0 ]; then
  echo
  echo "[FAILURE]: Something went wrong..."
else
  echo
  echo "[SUCCESS]: ${INPUT_TABLE} table has been loaded to Hive."
  echo
  echo "[SUCCESS]: Data stored in following directory: ${LANDING_DIR}"
fi

exit ${RETURN_CODE}
