# big_data_utilities

This repository houses application code and configuration files that demonstrate some tooling and capabilities related to Big Data, specifically:
* Sqoop 
* Spark 
* Jupyter Notebooks
* Python (converted directly from Jupyter Notebooks)